/*-----------------------------------*\
$ The Login System...
\*-----------------------------------*/
var login = false; // not logged in
var theForm = document.getElementById('theForm');
var theQuiz = document.getElementById('theQuiz');
var pass = document.getElementById('pass');
var submitBtn = document.getElementById('submit');
var err = document.getElementById('err');
var errH = document.getElementById('errH');

function typing() { // enable 'verify identity btn' as user starts typing
    if(pass.value != '') {
        submitBtn.removeAttribute('disabled');
        document.getElementsByClassName('finger-print')[0].style.opacity = '1';
    } else {
        submitBtn.setAttribute('disabled', 'disabled');
        document.getElementsByClassName('finger-print')[0].style.opacity = '.6';
    }
}

function chkPass(btn) { // checking password
    if(pass.value == 11223344) {
        pass.setAttribute('disabled', 'disabled');
        submitBtn.setAttribute('disabled', 'disabled');

        submitBtn.innerHTML = '<i class="material-icons loading" style="font-size: 1.8em;">cached</i>';

        login = true;

        setTimeout(function(){

            document.getElementsByClassName('finger-print')[0].style.display = 'none';
            document.getElementsByClassName('success')[0].style.display = 'block';
            document.getElementsByClassName('success')[0].style.opacity = '1';
            
            document.getElementsByClassName('passBox')[0].style.display = 'none';

            errH.innerHTML = 'Bingo!';

            err.style.color = '#28a745';
            err.innerHTML = 'The password was <br> Correct!';
            
            btn.innerHTML = 'Start Quiz &gt;';
            btn.removeAttribute('disabled');

            btn.classList.add('btn-success');
            btn.setAttribute('onclick', 'startQuiz();');

        }, 2000);
        
    } else if(pass.value == '') { // if in case someone puts empty pass

        document.getElementsByClassName('finger-print')[0].style.display = 'none';
        document.getElementsByClassName('warning')[0].style.display = 'block';
        document.getElementsByClassName('warning')[0].style.opacity = '1';

        errH.innerHTML = 'Errrr!';

        document.getElementsByClassName('passBox')[0].style.display = 'none';

        err.style.color = '#dc3545';
        err.innerHTML = 'Password Can\'t Be Empty!';

        btn.innerHTML = 'Reload';
        btn.removeAttribute('disabled');

        btn.classList.add('btn-danger');
        btn.setAttribute('onclick', 'window.location.reload()');
        
    } else { // if wrong password
        pass.setAttribute('disabled', 'disabled');
        btn.setAttribute('disabled', 'disabled');

        btn.innerHTML = '<i class="material-icons loading" style="font-size: 1.8em;">cached</i>';

        setTimeout(function(){

            document.getElementsByClassName('finger-print')[0].style.display = 'none';
            document.getElementsByClassName('warning')[0].style.display = 'block';
            document.getElementsByClassName('warning')[0].style.opacity = '1';
            
            document.getElementsByClassName('passBox')[0].style.display = 'none';

            errH.innerHTML = 'Errrr!';

            err.style.color = '#dc3545';
            err.innerHTML = 'The password was <br> NOT Correct!';
            
            btn.innerHTML = 'Dismiss';
            btn.removeAttribute('disabled');

            btn.classList.add('btn-danger');
            btn.setAttribute('onclick', 'window.location.reload();');

            pass.removeAttribute('disabled');
            btn.removeAttribute('disabled');

            errH.classList.add("shake");
            err.classList.add("shake");

        }, 2000);
    }   
}

function resetErr() { // reset all error to default (This is triggered when user focus on pass input box)
    pass.style.borderColor = '#007bff';
    pass.value = '';
    err.innerHTML = '';

    err.classList.remove("shake");
    pass.classList.remove("shake");

    document.getElementsByClassName('warning')[0].style.display = 'none';
    document.getElementsByClassName('finger-print')[0].style.display = 'block';
    document.getElementsByClassName('finger-print')[0].style.opacity = '.6';

    submitBtn.innerHTML = 'Confirm Identity';
    submitBtn.classList.remove("btn-danger");
    submitBtn.classList.add('btn-primary');
}

function startQuiz() {
    theForm.style.display = 'none';
    theQuiz.style.display = 'block';
}

/*-----------------------------------*\
$ The Quiz Begins...
\*-----------------------------------*/
var userAns = []; // Store all given answers
var currentTab = 0; // Current tab is set to be the first tab (0)
showTab(currentTab); // Display the crurrent tab

var nextBtn = document.getElementById('next-button');

function enableBtn(i) { // enable btn if radio btn is checked
    if(i.checked) {
        nextBtn.removeAttribute('disabled');
    } else {
        nextBtn.setAttribute('disabled', 'disabled');
    }
}
        
function showTab(n) { // display the specified tab of the form
    var x = document.getElementsByClassName("tab");
    x[n].style.display = "block";
    
    if (n == (x.length - 1)) { // dynamic next button
        document.getElementById('next-button').innerHTML = "Submit";
    } else {
        document.getElementById('next-button').innerHTML = "Next";
    }
    
    fixStepIndicator(n) // it will display the correct step indicator
}

function next() {  // figure out which tab to display
    var x = document.getElementsByClassName("tab");
    
    if (!validateForm()) return false; // exit if no option in the current tab is selected
    
    x[currentTab].style.display = "none"; //hide current tab
    
    currentTab = currentTab + 1; // Increase the current tab by 1

    if (currentTab >= x.length) { // if reached the end of the form
        // ... the form gets submitted:
        document.getElementById("quizForm").submit();
        return false;
    }
    
    showTab(currentTab); // otherwise, display the correct tab
}
        
function validateForm() { // deals with validation of radio options
    var x, qOpts, valid = false;
    x = document.getElementsByClassName("tab");
    qOpts = x[currentTab].getElementsByClassName("qOpts");

    for ( var i = 0; i < qOpts.length; i++) { // checks every radio btn in current tab
        if(qOpts[i].checked) { // if found checked
            valid = true;
            userAns.unshift(qOpts[i].value); // store user's answer
            console.log(userAns);

            nextBtn.setAttribute('disabled', 'disabled'); // disbale button for next question
            break;
        }
    }

    if(!valid)   { // if no option selected
        alert("Please Select Any Option...");
        nextBtn.setAttribute('disabled', 'disabled');
    }

    if (valid) { // if the valid status is true, mark the step as finished
        document.getElementsByClassName("step")[currentTab].className += " finish";
    }
    return valid; // return the valid status
}
            
function fixStepIndicator(n) { // removes the "active" class of all steps...
    var i, x = document.getElementsByClassName("step");

    for (i = 0; i < x.length; i++) {
        x[i].className = x[i].className.replace(" active", "");
    }
    
    x[n].className += " active"; // and adds the "active" class on the current step
}